package frc3838.Y2013;

import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.InternalButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2013.commands.CommandBase;
import frc3838.Y2013.commands.climberArms.DecreaseClimberArmsSpeedOnePercentCommand;
import frc3838.Y2013.commands.climberArms.DecreaseClimberArmsSpeedTenPercentCommand;
import frc3838.Y2013.commands.climberArms.IncreaseClimberArmsSpeedOnePercentCommand;
import frc3838.Y2013.commands.climberArms.IncreaseClimberArmsSpeedTenPercentCommand;
import frc3838.Y2013.commands.climberArms.MoveArmsDownCommand;
import frc3838.Y2013.commands.climberArms.MoveArmsUpCommand;
import frc3838.Y2013.commands.climberArms.StopArmsCommand;
import frc3838.Y2013.commands.drive.ToggleSensitivityAtLowSpeedsCommand;
import frc3838.Y2013.commands.feedback.ShowAutonomousOptionOnDashboardCommand;
import frc3838.Y2013.commands.gearbox.DisengageClimbModeCommand;
import frc3838.Y2013.commands.gearbox.EngageClimbModeCommand;
import frc3838.Y2013.commands.picker.LowerPickerSequence;
import frc3838.Y2013.commands.picker.PickerActivationSequence;
import frc3838.Y2013.commands.picker.RaisePickerSequence;
import frc3838.Y2013.commands.picker.SlowEjectCommandGroup;
import frc3838.Y2013.commands.picker.StartPickerMotorCommand;
import frc3838.Y2013.commands.picker.StopPickerMotorCommand;
import frc3838.Y2013.commands.shooting.ShootingSequenceCommandGroup;
import frc3838.Y2013.commands.shooting.StartShooterMotorsCommand;
import frc3838.Y2013.commands.shooting.StopShooterMotorsCommand;
import frc3838.Y2013.commands.shooting.angle.SetToHighShotCommand;
import frc3838.Y2013.commands.shooting.angle.SetToLowShotCommand;
import frc3838.Y2013.commands.shooting.speed.DecreaseBothShooterMotorsOnePercentCommand;
import frc3838.Y2013.commands.shooting.speed.DecreaseBothShooterMotorsTenPercentCommand;
import frc3838.Y2013.commands.shooting.speed.DecreaseLowerShooterMotorOnePercentCommand;
import frc3838.Y2013.commands.shooting.speed.DecreaseLowerShooterMotorTenPercentCommand;
import frc3838.Y2013.commands.shooting.speed.DecreaseUpperShooterMotorOnePercentCommand;
import frc3838.Y2013.commands.shooting.speed.DecreaseUpperShooterMotorTenPercentCommand;
import frc3838.Y2013.commands.shooting.speed.IncreaseBothShooterMotorsOnePercentCommand;
import frc3838.Y2013.commands.shooting.speed.IncreaseBothShooterMotorsTenPercentCommand;
import frc3838.Y2013.commands.shooting.speed.IncreaseLowerShooterMotorOnePercentCommand;
import frc3838.Y2013.commands.shooting.speed.IncreaseLowerShooterMotorTenPercentCommand;
import frc3838.Y2013.commands.shooting.speed.IncreaseUpperShooterMotorOnePercentCommand;
import frc3838.Y2013.commands.shooting.speed.IncreaseUpperShooterMotorTenPercentCommand;
import frc3838.Y2013.commands.time.SleepMillisecondsCommand;
import frc3838.Y2013.components.DigitalInputTriggeredButton;
import frc3838.Y2013.subsystems.AutonomousOptionSubsystem;
import frc3838.Y2013.subsystems.ClimberArmsSubsystem;
import frc3838.Y2013.subsystems.DriveTrainSubsystem;
import frc3838.Y2013.subsystems.FiringPinSubsystem;
import frc3838.Y2013.subsystems.PickerSubsystem;
import frc3838.Y2013.subsystems.ShooterAngleSubsystem;
import frc3838.Y2013.subsystems.ShooterMotorsSubsystem;
import frc3838.Y2013.utils.LOG;



/** @noinspection FieldCanBeLocal */
public class OI2013
{

    private Button fireButton;
    private Button driveSensitivityButton;

    private Button engageClimbModeButton;
    private Button disengageClimbModeButton;


    private Button startShooterMotors;
    private Button stopShooterMotors;

    private Button changeToHighShot;
    private Button changeToLowShot;

    private Button toggleStickModeButton;

    private Button climberArmDownButton;
    private Button climberArmUpButton;

    private Button climberArmIncreaseSpeedOneButton;
    private Button climberArmIncreaseSpeedTenButton;
    private Button climberArmDecreaseSpeedOneButton;
    private Button climberArmDecreaseSpeedTenButton;


    private Button increaseBothShooterMotorsOneButton;
    private Button increaseBothShooterMotorsTenButton;
    private Button decreaseBothShooterMotorsOneButton;
    private Button decreaseBothShooterMotorsTenButton;

    private Button increaseLowerShooterMotorsOneButton;
    private Button increaseLowerShooterMotorsTenButton;

    private Button decreaseLowerShooterMotorsOneButton;
    private Button decreaseLowerShooterMotorsTenButton;

    private Button increaseUpperShooterMotorsOneButton;
    private Button increaseUpperShooterMotorsTenButton;

    private Button decreaseUpperShooterMotorsOneButton;
    private Button decreaseUpperShooterMotorsTenButton;

    private Button autonomousOptionDisplayButton;

    private Button shooterMotorSpeedUpOne;
    private Button shooterMotorSpeedDownOne;
    private Button shooterMotorSpeedUpTen;
    private Button shooterMotorSpeedDownTen;

    private Button raisePickerButton;
    private Button lowerPickerButton;

    private Button startPickerMotor;
    private Button stopPickerMotor;
    private Button slowEjectPickerMotor;
    private Button pulseEjectPickerMotor;

    private Button pickerActivationSwitchButton;


    public OI2013()
    {
        LOG.debug("Entering OI2013 Constructor");
        initAutonomousOptionDisplayControl();
        initClimberArmControls();
        initClimbModeControls();
        initDriveSensitivityControls();
        initDriveStickModeControls();
        initFiringPinControls();
        initPickerControls();
        initShooterMotorControls();
        initShooterAngleControls();
        initShooterMotorSpeedControls();
        initShooterMotorCalibrationControls();
        LOG.debug("Exiting OI2013 Constructor");
    }


    private void initAutonomousOptionDisplayControl()
    {
        try
        {
            if (AutonomousOptionSubsystem.isEnabled)
            {
                autonomousOptionDisplayButton = new InternalButton();
                autonomousOptionDisplayButton.whenPressed(new ShowAutonomousOptionOnDashboardCommand());
                if (RobotMap2013.IN_DEBUG_MODE)
                {
                    SmartDashboard.putData("Display Auto State", autonomousOptionDisplayButton);
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initAutonomousOptionDisplayControl()", e);
        }
    }


    private void initPickerControls()
    {
        try
        {
            if (PickerSubsystem.isEnabled)
            {
                lowerPickerButton = RobotMap2013.Buttons.LOWER_PICKER;
                lowerPickerButton.whenPressed(new LowerPickerSequence());

                raisePickerButton = RobotMap2013.Buttons.RAISE_PICKER;
                raisePickerButton.whenPressed(new RaisePickerSequence());

                startPickerMotor = RobotMap2013.Buttons.START_PICKER_MOTOR;
                startPickerMotor.whenPressed(new StartPickerMotorCommand());

                stopPickerMotor = RobotMap2013.Buttons.STOP_PICKER_MOTOR;
                stopPickerMotor.whenPressed(new StopPickerMotorCommand());

                pickerActivationSwitchButton = new DigitalInputTriggeredButton(CommandBase.pickerSubsystem.getActivateSwitch());
                pickerActivationSwitchButton.whenPressed(new PickerActivationSequence());

                slowEjectPickerMotor = RobotMap2013.Buttons.SLOW_PICKER_EJECT;
//                slowEjectPickerMotor.whenPressed(new StartPickerMotorSlowCommand());
//                slowEjectPickerMotor.whenPressed(new SlowEjectPulsingCommand());
                slowEjectPickerMotor.whenPressed(new SlowEjectCommandGroup());

                pulseEjectPickerMotor = RobotMap2013.Buttons.PULSE_HOLD_EJECT;
                pulseEjectPickerMotor.whileHeld(new SlowEjectCommandGroup());
                pulseEjectPickerMotor.whenReleased(new StopPickerMotorCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initPickerControls()", e);
        }

    }


    private void initClimberArmControls()
    {
        try
        {
            if (ClimberArmsSubsystem.isEnabled)
            {
                LOG.trace("Initializing Drive Climber Arm Controls");
                climberArmDownButton = RobotMap2013.Buttons.CLIMBER_ARM_DOWN;
                climberArmDownButton.whileHeld(new MoveArmsDownCommand());
                climberArmDownButton.whenReleased(new StopArmsCommand());

                climberArmUpButton = RobotMap2013.Buttons.CLIMBER_ARM_UP;
                climberArmUpButton.whileHeld(new MoveArmsUpCommand());
                climberArmUpButton.whenReleased(new StopArmsCommand());

                if (ClimberArmsSubsystem.isEnabled && ClimberArmsSubsystem.isInCalibrateMode)
                {
                    climberArmIncreaseSpeedOneButton = new InternalButton();
                    climberArmIncreaseSpeedOneButton.whenPressed(new IncreaseClimberArmsSpeedOnePercentCommand());
                    SmartDashboard.putData("Climber Arm +1%", climberArmIncreaseSpeedOneButton);

                    climberArmIncreaseSpeedTenButton = new InternalButton();
                    climberArmIncreaseSpeedTenButton.whenPressed(new IncreaseClimberArmsSpeedTenPercentCommand());
                    SmartDashboard.putData("Climber Arm +10%", climberArmIncreaseSpeedTenButton);

                    climberArmDecreaseSpeedOneButton = new InternalButton();
                    climberArmDecreaseSpeedOneButton.whenPressed(new DecreaseClimberArmsSpeedOnePercentCommand());
                    SmartDashboard.putData("Climber Arm -1%", climberArmDecreaseSpeedOneButton);

                    climberArmDecreaseSpeedTenButton = new InternalButton();
                    climberArmDecreaseSpeedTenButton.whenPressed(new DecreaseClimberArmsSpeedTenPercentCommand());
                    SmartDashboard.putData("Climber Arm -10%", climberArmDecreaseSpeedTenButton);
                }


            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initClimberArmControls()", e);
        }
    }


    private void initDriveSensitivityControls()
    {
        try
        {
            if (DriveTrainSubsystem.isEnabled)
            {
                LOG.trace("Initializing Drive Sensitivity Toggle Control");
                driveSensitivityButton = RobotMap2013.Buttons.SENSITIVITY_TOGGLE;
                driveSensitivityButton.whenPressed(new ToggleSensitivityAtLowSpeedsCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initDriveSensitivityControls()", e);
        }
    }


    private void initDriveStickModeControls()
    {
        try
        {
            if (DriveTrainSubsystem.isEnabled)
            {
                toggleStickModeButton = RobotMap2013.Buttons.TOGGLE_STICK_MODE;
//                toggleStickModeButton.whenPressed(new ToggleStickModeCommand());
                toggleStickModeButton.whenPressed(new SleepMillisecondsCommand(1));
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initDriveStickModeControls()", e);
        }

    }


    private void initClimbModeControls()
    {
        try
        {
            if (DriveTrainSubsystem.isClimbSystemEnabled)
            {
                engageClimbModeButton = RobotMap2013.Buttons.ENGAGE_CLIMB_MODE;
                engageClimbModeButton.whenPressed(new EngageClimbModeCommand());

                disengageClimbModeButton = RobotMap2013.Buttons.DISENGAGE_CLIMB_MODE;
                disengageClimbModeButton.whenPressed(new DisengageClimbModeCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initClimbModeControls()", e);
        }

    }


    private void initFiringPinControls()
    {
        try
        {
            if (FiringPinSubsystem.isEnabled)
            {
                LOG.trace("Initializing Fire and Shoot Button");
                fireButton = RobotMap2013.Buttons.SHOOT;
                fireButton.whenPressed(new ShootingSequenceCommandGroup());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when OI2013.initFiringPinControls was executed.", e);
        }
    }


    private void initShooterAngleControls()
    {
        try
        {
            if (ShooterAngleSubsystem.isEnabled)
            {
                changeToHighShot = RobotMap2013.Buttons.CHANGE_TO_HIGH_SHOT;
                changeToHighShot.whenPressed(new SetToHighShotCommand());

                changeToLowShot = RobotMap2013.Buttons.CHANGE_TO_LOW_SHOT;
                changeToLowShot.whenPressed(new SetToLowShotCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initShooterAngleControls()", e);
        }

    }


    private void initShooterMotorControls()
    {
        try
        {
            if (ShooterMotorsSubsystem.isEnabled)
            {
                startShooterMotors = RobotMap2013.Buttons.START_SHOOTER_MOTORS;
                stopShooterMotors = RobotMap2013.Buttons.STOP_SHOOTER_MOTORS;

                startShooterMotors.whenPressed(new StartShooterMotorsCommand());
                stopShooterMotors.whenPressed(new StopShooterMotorsCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initShooterMotorControls()", e);
        }

    }


    private void initShooterMotorSpeedControls()
    {

        try
        {
            if (ShooterMotorsSubsystem.isEnabled)
            {
                shooterMotorSpeedDownOne = RobotMap2013.Buttons.SHOOTER_SPEED_DOWN_ONE;
                shooterMotorSpeedDownOne.whenPressed(new DecreaseBothShooterMotorsOnePercentCommand());
                shooterMotorSpeedUpOne = RobotMap2013.Buttons.SHOOTER_SPEED_UP_ONE;
                shooterMotorSpeedUpOne.whenPressed(new IncreaseBothShooterMotorsOnePercentCommand());

                shooterMotorSpeedDownTen = RobotMap2013.Buttons.SHOOTER_SPEED_DOWN_TEN;
                shooterMotorSpeedDownTen.whenPressed(new DecreaseBothShooterMotorsTenPercentCommand());
                shooterMotorSpeedUpTen = RobotMap2013.Buttons.SHOOTER_SPEED_UP_TEN;
                shooterMotorSpeedUpTen.whenPressed(new IncreaseBothShooterMotorsTenPercentCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initShooterMotorSpeedControls()", e);
        }

    }


    private void initShooterMotorCalibrationControls()
    {
        try
        {
            if (ShooterMotorsSubsystem.isEnabled && ShooterMotorsSubsystem.isInCalibrateMode)
            {
                increaseBothShooterMotorsOneButton = new InternalButton();
                increaseBothShooterMotorsOneButton.whenPressed(new IncreaseBothShooterMotorsOnePercentCommand());
                SmartDashboard.putData("Both +1%", increaseBothShooterMotorsOneButton);
                increaseBothShooterMotorsTenButton = new InternalButton();
                increaseBothShooterMotorsTenButton.whenPressed(new IncreaseBothShooterMotorsTenPercentCommand());
                SmartDashboard.putData("Both +10%", increaseBothShooterMotorsTenButton);

                decreaseBothShooterMotorsOneButton = new InternalButton();
                decreaseBothShooterMotorsOneButton.whenPressed(new DecreaseBothShooterMotorsOnePercentCommand());
                SmartDashboard.putData("Both -1%", decreaseBothShooterMotorsOneButton);

                decreaseBothShooterMotorsTenButton = new InternalButton();
                decreaseBothShooterMotorsTenButton.whenPressed(new DecreaseBothShooterMotorsTenPercentCommand());
                SmartDashboard.putData("Both -10%", decreaseBothShooterMotorsOneButton);


                increaseLowerShooterMotorsOneButton = new InternalButton();
                increaseLowerShooterMotorsOneButton.whenPressed(new IncreaseLowerShooterMotorOnePercentCommand());
                SmartDashboard.putData("Lower +1%", increaseLowerShooterMotorsOneButton);

                increaseLowerShooterMotorsTenButton = new InternalButton();
                increaseLowerShooterMotorsTenButton.whenPressed(new IncreaseLowerShooterMotorTenPercentCommand());
                SmartDashboard.putData("Lower +10%", increaseLowerShooterMotorsTenButton);

                decreaseLowerShooterMotorsOneButton = new InternalButton();
                decreaseLowerShooterMotorsOneButton.whenPressed(new DecreaseLowerShooterMotorOnePercentCommand());
                SmartDashboard.putData("Lower -1%", decreaseLowerShooterMotorsOneButton);

                decreaseLowerShooterMotorsTenButton = new InternalButton();
                decreaseLowerShooterMotorsTenButton.whenPressed(new DecreaseLowerShooterMotorTenPercentCommand());
                SmartDashboard.putData("Lower -10%", decreaseLowerShooterMotorsTenButton);


                increaseUpperShooterMotorsOneButton = new InternalButton();
                increaseUpperShooterMotorsOneButton.whenPressed(new IncreaseUpperShooterMotorOnePercentCommand());
                SmartDashboard.putData("Upper +1%", increaseUpperShooterMotorsOneButton);

                increaseUpperShooterMotorsTenButton = new InternalButton();
                increaseUpperShooterMotorsTenButton.whenPressed(new IncreaseUpperShooterMotorTenPercentCommand());
                SmartDashboard.putData("Upper +10%", increaseUpperShooterMotorsTenButton);

                decreaseUpperShooterMotorsOneButton = new InternalButton();
                decreaseUpperShooterMotorsOneButton.whenPressed(new DecreaseUpperShooterMotorOnePercentCommand());
                SmartDashboard.putData("Upper -1%", decreaseUpperShooterMotorsOneButton);

                decreaseUpperShooterMotorsTenButton = new InternalButton();
                decreaseUpperShooterMotorsTenButton.whenPressed(new DecreaseUpperShooterMotorTenPercentCommand());
                SmartDashboard.putData("Upper -10%", decreaseUpperShooterMotorsTenButton);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2013.initShooterMotorCalibrationControls()", e);
        }

    }

}


//// CREATING BUTTONS
// One type of button is a joystick button which is any button on a joystick.
// You create one by telling it which joystick it's on and which button
// number it is.
// Joystick stick = new Joystick(port);
// Button button = new JoystickButton(stick, buttonNumber);

// Another type of button you can create is a DigitalIOButton, which is
// a button or switch hooked up to the cypress module. These are useful if
// you want to build a customized operator interface.
// Button button = new DigitalIOButton(1);

// There are a few additional built in buttons you can use. Additionally,
// by subclassing Button you can create custom triggers and bind those to
// commands the same as any other Button.

//// TRIGGERING COMMANDS WITH BUTTONS
// Once you have a button, it's trivial to bind it to a button in one of
// three ways:

// Start the command when the button is pressed and let it run the command
// until it is finished as determined by it's isFinished method.
// button.whenPressed(new ExampleCommand());

// Run the command while the button is being held down and interrupt it once
// the button is released.
// button.whileHeld(new ExampleCommand());