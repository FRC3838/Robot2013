package frc3838.Y2013.components;


import edu.wpi.first.wpilibj.DigitalInput;



public class TriSwitch
{


    private DigitalInput input1;
    private DigitalInput input2;


    public TriSwitch(int digitalInput1Channel, int digitalInput2Channel)
    {
        this(new DigitalInput(digitalInput1Channel), new DigitalInput(digitalInput2Channel));
    }


    public TriSwitch(DigitalInput input1, DigitalInput input2)
    {
        this.input1 = input1;
        this.input2 = input2;
    }


    public TriSwitchPosition getState()
    {
        TriSwitchPosition state;
        if (input1.get() && !input2.get())
        {
            state = TriSwitchPosition.FlippedRight;
        }
        else if (input2.get() && !input1.get())
        {
            state = TriSwitchPosition.FlippedLeft;
        }
        else
        {
            state = TriSwitchPosition.CenterPosition;
        }

        return state;
    }


    public String getStateDetails() {return getState().getName() + " :: " + "1:" + input1.get() + "  2:" + input2.get();}
}
