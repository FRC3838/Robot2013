package frc3838.Y2013.components;


import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.command.Command;



public class JoystickShiftedButton
{
    private GenericHID joystick;
    private ButtonDelegate buttonDelegate;
    private ShiftButtonDelegate shiftedButtonDelegate;
    
    
    /**
     * Create a joystick button for triggering commands
     *
     * @param joystick     The GenericHID object that has the button (e.g. Joystick, KinectStick, etc)
     * @param buttonNumber  The button number (see {@link GenericHID#getRawButton(int) }
     * @param shiftButtonNumber  The button number that acts as the modifier
     */
    public JoystickShiftedButton(GenericHID joystick, int buttonNumber, int shiftButtonNumber)
    {
        this.joystick = joystick;
        
        buttonDelegate = new ButtonDelegate(buttonNumber, shiftButtonNumber);
//        buttonBDelegate = new ButtonDelegate(buttonNumber, buttonNumber);
        shiftedButtonDelegate = new ShiftButtonDelegate(buttonNumber, shiftButtonNumber);
    }

    public void whenPressed(Command command)
    {
        buttonDelegate.whenPressed(command);
    }

    
    
    public void whenPressedShifted(Command command)
    {
        shiftedButtonDelegate.whenPressed(command);
    }

    public void whenReleased(Command command)
    {
        buttonDelegate.whenReleased(command);
    }
  

    public void whenReleasedShifted(Command command)
    {
        shiftedButtonDelegate.whenReleased(command);
    }

    public void whileHeld(Command command)
    {
        buttonDelegate.whileHeld(command);
    }
 

    public void whileHeldShifted(Command command)
    {
        shiftedButtonDelegate.whileHeld(command);
    }

    private class ShiftButtonDelegate extends Button
    {
        public int buttonNumber;
        public int buttonBNumber;

        private ShiftButtonDelegate(int buttonNumber, int buttonBNumber)
        {
            this.buttonNumber = buttonNumber;
            this.buttonBNumber = buttonBNumber;
        }

        public boolean get()
        {
            return joystick.getRawButton(buttonNumber) && joystick.getRawButton(buttonBNumber);
        }
    }

    private class ButtonDelegate extends Button
    {
        private int buttonNumber;
        private int alternateButtonNumber;

        private ButtonDelegate(int buttonNumber, int alternateButtonNumber)
        {
            this.buttonNumber = buttonNumber;
            this.alternateButtonNumber = alternateButtonNumber;
        }

        public boolean get()
        {
            return joystick.getRawButton(buttonNumber) && !joystick.getRawButton(alternateButtonNumber);
        }
    }
}
