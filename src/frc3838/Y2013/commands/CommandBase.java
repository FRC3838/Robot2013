package frc3838.Y2013.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc3838.Y2013.OI2013;
import frc3838.Y2013.subsystems.AutonomousOptionSubsystem;
import frc3838.Y2013.subsystems.ClimberArmsSubsystem;
import frc3838.Y2013.subsystems.CompressorSubsystem;
import frc3838.Y2013.subsystems.DriveTrainSubsystem;
import frc3838.Y2013.subsystems.FiringPinSubsystem;
import frc3838.Y2013.subsystems.PickerSubsystem;
import frc3838.Y2013.subsystems.ShooterAngleSubsystem;
import frc3838.Y2013.subsystems.ShooterMotorsSubsystem;
import frc3838.Y2013.utils.LOG;



/**
 * The base for all commands. All atomic commands should subclass CommandBase. CommandBase stores creates and stores each control system. To access a subsystem elsewhere in your
 * code in your code use CommandBase.exampleSubsystem
 *
 * @author Author
 */
public abstract class CommandBase extends Command
{
    /** @noinspection NonConstantFieldWithUpperCaseName */
    protected static OI2013 OI2013;
    // Create a single static instance of all of your subsystems here; initialize them in the init method below

    protected static final AutonomousOptionSubsystem autonomousOptionSubsystem = AutonomousOptionSubsystem.getInstance();
    protected static final ClimberArmsSubsystem climberArmsSubsystem = ClimberArmsSubsystem.getInstance();
    protected static final CompressorSubsystem compressorSubsystem = CompressorSubsystem.getInstance();
    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
    protected static final FiringPinSubsystem firingPinSubsystem = FiringPinSubsystem.getInstance();
    public static final PickerSubsystem pickerSubsystem = PickerSubsystem.getInstance();
    protected static final ShooterAngleSubsystem shooterAngleSubsystem = ShooterAngleSubsystem.getInstance();
    protected static final ShooterMotorsSubsystem shooterMotorsSubsystem = ShooterMotorsSubsystem.getInstance();


    /** @noinspection ConstructorNotProtectedInAbstractClass */
    public CommandBase(String name)
    {
        super(name);
    }


    /** @noinspection ConstructorNotProtectedInAbstractClass */
    public CommandBase()
    {
        super();
    }


    /** Initializes all the subsystems. This is called by the robotInit() method in the primary Robot class 'TheRobot2013'. */
    public static void init()
    {
        LOG.trace("Entering CommandBase.init()");
        // This MUST be here. If the OI2013 creates Commands (which it very likely
        // will), constructing it during the construction of CommandBase (from
        // which commands extend), subsystems are not guaranteed to be
        // yet. Thus, their requires() statements may grab null pointers. Bad
        // news. Don't move it.
        LOG.trace("Calling OI2013 constructor");
        OI2013 = new OI2013();
        LOG.trace("Returned from OI2013 Constructor call");

        // Show what command your subsystem is running on the SmartDashboard
        //SmartDashboard.putData(exampleSubsystem);


        LOG.trace("Exiting CommandBase.init()");
    }


}
