package frc3838.Y2013.commands.picker;


import edu.wpi.first.wpilibj.command.CommandGroup;
import frc3838.Y2013.commands.time.SleepMillisecondsCommand;



public class SlowEjectCommandGroup extends CommandGroup
{
    public SlowEjectCommandGroup()
    {
        super();
        addCommands();
    }


    public SlowEjectCommandGroup(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {
        addSequential(new StartPickerMotorSlowCommand());
        addSequential(new SleepMillisecondsCommand(100));
        addSequential(new StopPickerMotorCommand());
        addSequential(new SleepMillisecondsCommand(100));
    }


    protected boolean isFinished()
    {
        return super.isFinished();
    }


    // In a constructor...
    // Add Commands:
    // e.g. addSequential(new Command1());
    //      addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    //      addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.
}