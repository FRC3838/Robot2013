package frc3838.Y2013.commands.picker;

import frc3838.Y2013.subsystems.PickerSubsystem;
import frc3838.Y2013.utils.LOG;



public class SlowEjectPulsingCommand extends frc3838.Y2013.commands.CommandBase
{

    private long startTime;
    private final long runDelay = 50;
    private final long stopDelay = runDelay;
    private boolean finished = false;

    private boolean isRunning = false;

    private boolean firstRun = true;


    public SlowEjectPulsingCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(pickerSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        firstRun = true;
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (PickerSubsystem.isEnabled)
        {
            try
            {
                if (firstRun)
                {
                    pickerSubsystem.slowEject();
                    startTimer();
                    isRunning = true;
                    firstRun = false;
                }
                else
                {
                    calc();
                    if (finished)
                    {
                        if (isRunning)
                        {
                            pickerSubsystem.pauseMotor();
                        }
                        else
                        {
                            pickerSubsystem.slowEject();
                        }
                        startTimer();
                    }
                }
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in SlowEjectPulsingCommand.execute()", e);
            }
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return !pickerSubsystem.isPulsing();
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {

    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }


    private void startTimer()
    {
        finished = false;
        startTime = System.currentTimeMillis();
    }


    protected void calc()
    {
        long diff = System.currentTimeMillis() - startTime;
        finished = diff >= runDelay;
    }
}
