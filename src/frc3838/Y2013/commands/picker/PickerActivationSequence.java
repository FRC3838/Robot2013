package frc3838.Y2013.commands.picker;


import edu.wpi.first.wpilibj.command.CommandGroup;
import frc3838.Y2013.commands.shooting.angle.SetToHighShotCommand;



public class PickerActivationSequence extends CommandGroup
{
    public PickerActivationSequence()
    {
        super();
        addCommands();
    }


    public PickerActivationSequence(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {
        addSequential(new StopPickerMotorCommand());
        addSequential(new RaisePickerCommand());
        addSequential(new SetToHighShotCommand());
    }

    // In a constructor...
    // Add Commands:
    // e.g. addSequential(new Command1());
    //      addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    //      addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.
}