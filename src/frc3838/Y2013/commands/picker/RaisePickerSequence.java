package frc3838.Y2013.commands.picker;


import edu.wpi.first.wpilibj.command.CommandGroup;



public class RaisePickerSequence extends CommandGroup
{
    public RaisePickerSequence()
    {
        super();
        addCommands();
    }


    public RaisePickerSequence(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {
        addSequential(new RaisePickerCommand());
        addSequential(new StopPickerMotorCommand());
    }
}