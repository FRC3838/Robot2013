package frc3838.Y2013.commands.picker;


import edu.wpi.first.wpilibj.command.CommandGroup;



public class LowerPickerSequence extends CommandGroup
{
    public LowerPickerSequence()
    {
        super();
        addCommands();
    }


    public LowerPickerSequence(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {
        addSequential(new StartPickerMotorCommand());
        addSequential(new LowerPickerCommand());
    }
}