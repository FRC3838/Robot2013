package frc3838.Y2013.commands.drive;


import frc3838.Y2013.commands.CommandBase;
import frc3838.Y2013.subsystems.DriveTrainSubsystem;
import frc3838.Y2013.utils.LOG;



public class DecreasedSensitivityAtLowerSpeedsCommand extends CommandBase
{

    public DecreasedSensitivityAtLowerSpeedsCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(driveTrainSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (DriveTrainSubsystem.isEnabled)
        {
            try
            {
                driveTrainSubsystem.setDecreaseSensitivityAtLowerSpeeds(true);
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in DecreasedSensitivityAtLowerSpeedsCommand.execute()", e);
            }

        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
