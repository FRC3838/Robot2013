package frc3838.Y2013.commands.feedback;


import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2013.commands.CommandBase;



public class JoystickAssignmentDisplayCommand extends CommandBase
{
    private String joystickName;
    private int expectedPort;
    
    
    public JoystickAssignmentDisplayCommand(String joystickName, int expectedPort)
    {
        this.joystickName = joystickName;
        this.expectedPort = expectedPort;
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        SmartDashboard.putString("Stick", expectedPort + " : " + joystickName);
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
