package frc3838.Y2013.commands.compressor;


import frc3838.Y2013.commands.CommandBase;
import frc3838.Y2013.subsystems.CompressorSubsystem;
import frc3838.Y2013.utils.LOG;



public class RunCompressorCommand extends CommandBase
{
    private static boolean DEBUG_COMPRESSOR = false;


    public RunCompressorCommand()
    {
        requires(compressorSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        compressorSubsystem.startCompressor();
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (CompressorSubsystem.isEnabled)
        {
            //We do not need to do anything repeatedly. This command simply needs to initialize, and then end
            if (DEBUG_COMPRESSOR)
            {
                LOG.debug("Compressor Digital Switch Value: " + compressorSubsystem.getPressureSwitchValue());
            }
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
        LOG.trace("RunCompressorCommand end() method called. Stopping compressor.");
        compressorSubsystem.stopCompressor();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
