package frc3838.Y2013.commands.autonomous;


import edu.wpi.first.wpilibj.command.CommandGroup;
import frc3838.Y2013.commands.shooting.PostFireResetCommand;
import frc3838.Y2013.commands.shooting.ShootingSequenceCommandGroup;
import frc3838.Y2013.commands.shooting.StartShooterMotorsCommand;
import frc3838.Y2013.commands.shooting.StopShooterMotorsCommand;
import frc3838.Y2013.commands.shooting.angle.SetToHighShotCommand;
import frc3838.Y2013.commands.time.SleepSecondsCommand;
import frc3838.Y2013.utils.LOG;



public class AutonomousRightCommandGroup extends CommandGroup
{
    public AutonomousRightCommandGroup()
    {
        super();
        addCommands();
    }


    public AutonomousRightCommandGroup(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {
        LOG.info("Running " + getClass().getName());
        addSequential(new StartShooterMotorsCommand());
        addSequential(new SetToHighShotCommand());
        addSequential(new PostFireResetCommand());
        addSequential(new SleepSecondsCommand(3));
        //First Shot
        addSequential(new ShootingSequenceCommandGroup());
        int shooterDelay = 2;
        addSequential(new SleepSecondsCommand(shooterDelay));
        //Second Shot
        addSequential(new ShootingSequenceCommandGroup());
        addSequential(new SleepSecondsCommand(shooterDelay));
        //Third Shot
        addSequential(new ShootingSequenceCommandGroup());
        addSequential(new SleepSecondsCommand(shooterDelay));

        //First Dry Fire Shot
        addSequential(new ShootingSequenceCommandGroup());
        addSequential(new SleepSecondsCommand(shooterDelay));

        //Second Dry Fire Shot
        addSequential(new ShootingSequenceCommandGroup());
        addSequential(new SleepSecondsCommand(shooterDelay));

        addSequential(new StopShooterMotorsCommand());


    }

    // In a constructor...
    // Add Commands:
    // e.g. addSequential(new Command1());
    //      addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    //      addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.
}