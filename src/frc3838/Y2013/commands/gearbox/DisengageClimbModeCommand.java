package frc3838.Y2013.commands.gearbox;


import frc3838.Y2013.commands.CommandBase;
import frc3838.Y2013.subsystems.DriveTrainSubsystem;
import frc3838.Y2013.utils.LOG;



public class DisengageClimbModeCommand extends CommandBase
{

    public DisengageClimbModeCommand()
    {
        requires(driveTrainSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (DriveTrainSubsystem.isEnabled)
        {
            try
            {
                driveTrainSubsystem.disengageClimbMode();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in DisengageClimbModeCommand.execute()", e);
            }
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
