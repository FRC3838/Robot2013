package frc3838.Y2013.subsystems;


import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.commands.compressor.RunCompressorCommand;
import frc3838.Y2013.utils.LOG;



public class CompressorSubsystem extends Subsystem
{

    public static boolean isEnabled = true;

    private static final CompressorSubsystem singleton = new CompressorSubsystem();

    private static Compressor compressor;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such as <tt>new CompressorSubsystem()</tt> in order to use ensure only a
     * single instance of a Subsystem is created. This is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     CompressorSubsystem subsystem = new CompressorSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     CompressorSubsystem subsystem = CompressorSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static CompressorSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern). Use the static {@link #getInstance()} method to obtain a reference to the subsystem. For
     * example, instead of doing this:
     * <pre>
     *     CompressorSubsystem subsystem = new CompressorSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     CompressorSubsystem subsystem = CompressorSubsystem.getInstance();
     * </pre>
     */
    private CompressorSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in CompressorSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled)
        {
            try
            {
                LOG.debug("Initializing CompressorSubsystem");

                compressor = new Compressor(RobotMap2013.DigitalIO.COMPRESSOR_PRESSURE_SWITCH_CHANNEL,
                                            RobotMap2013.Relays.COMPRESSOR_RELAY_CHANNEL);
                LOG.debug("CompressorSubsystem completed successfully.");

            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in CompressorSubsystem.init()", e);
            }

        }
        else
        {
            LOG.info("CompressorSubsystem is disabled and will not be initialized");
        }
    }


    public void startCompressor()
    {
        try
        {
            if (isEnabled && compressor != null)
            {
                compressor.start();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in CompressorSubsystem.startCompressor()", e);
        }
    }


    public void startCompressorAndAutoRun()
    {
        setDefaultCommand(new RunCompressorCommand());
    }


    public void stopCompressor()
    {
        try
        {
            if (isEnabled && compressor != null)
            {
                compressor.stop();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in CompressorSubsystem.stopCompressor()", e);
        }
    }


    public boolean getPressureSwitchValue()
    {
        return compressor != null && compressor.getPressureSwitchValue();
    }


    public void initDefaultCommand()
    {
        //setDefaultCommand(new RunCompressorCommand());
    }
}
