package frc3838.Y2013.subsystems;


import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.utils.LOG;
import frc3838.Y2013.utils.math.Math2;



public class ClimberArmsSubsystem extends Subsystem
{

    public static boolean isEnabled = true;
    public static boolean isInCalibrateMode = false;

    private static final ClimberArmsSubsystem singleton = new ClimberArmsSubsystem();

    private static double speed = 0.28;

    private static final String DASHBOARD_LABEL = "Arms Move";
    private MotorOps armsMotorOps;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such as <tt>new ClimberArmsSubsystem()</tt> in order to use ensure only a
     * single instance of a Subsystem is created. This is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     ClimberArmsSubsystem subsystem = new ClimberArmsSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     ClimberArmsSubsystem subsystem = ClimberArmsSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static ClimberArmsSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern). Use the static {@link #getInstance()} method to obtain a reference to the subsystem. For
     * example, instead of doing this:
     * <pre>
     *     ClimberArmsSubsystem subsystem = new ClimberArmsSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     ClimberArmsSubsystem subsystem = ClimberArmsSubsystem.getInstance();
     * </pre>
     */
    private ClimberArmsSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in ClimberArmsSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled)
        {
            try
            {
                LOG.debug("Initializing ClimberArmsSubsystem");
                Victor armsMotor = new Victor(RobotMap2013.PWMChannels.CLIMBER_ARMS_MOTOR);
                armsMotorOps = new MotorOps(armsMotor, false);
                LOG.debug("ClimberArmsSubsystem initialization completed successfully");

            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in ClimberArmsSubsystem.init()", e);
            }

        }
        else
        {
            LOG.info("ClimberArmsSubsystem is disabled and will not be initialized");
        }
    }


    public void increaseSpeedOnePercent()
    {
        speed = armsMotorOps.constrainValue(speed + 0.01);
        displaySpeedSetting();
    }


    public void decreaseSpeedOnePercent()
    {
        speed = armsMotorOps.constrainValue(speed - 0.01);
        displaySpeedSetting();
    }


    public void increaseSpeedTenPercent()
    {
        speed = armsMotorOps.constrainValue(speed + 0.1);
        displaySpeedSetting();
    }


    public void decreaseSpeedTenPercent()
    {
        speed = armsMotorOps.constrainValue(speed - 0.1);
        displaySpeedSetting();
    }


    public void moveArmsDown()
    {
        final String value = armsMotorOps.setSpeed(speed);
        updateSpeedDisplay(value);
    }


    public void displaySpeedSetting()
    {
        SmartDashboard.putString("Arms Set:", Math2.toPercentage(speed));
    }


    public void moveArmsUp()
    {
        final String value = armsMotorOps.setSpeed(-(speed + .02));
        updateSpeedDisplay(value);
    }


    public void stopArms()
    {
        final String value = armsMotorOps.stop();
        updateSpeedDisplay(value);
    }


    private void updateSpeedDisplay(String value) {SmartDashboard.putString(DASHBOARD_LABEL, value);}


    public void initDefaultCommand()
    {
    }
}
