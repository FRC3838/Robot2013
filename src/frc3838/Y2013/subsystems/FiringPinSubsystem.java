package frc3838.Y2013.subsystems;


import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.components.SolenoidSubsystem;



public class FiringPinSubsystem extends SolenoidSubsystem
{
    public static boolean isEnabled = true;
    private static final FiringPinSubsystem singleton = new FiringPinSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such as <tt>new FiringPinSubsystem()</tt> in order to use ensure only a
     * single instance of a Subsystem is created. This is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     FiringPinSubsystem subsystem = new FiringPinSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     FiringPinSubsystem subsystem = FiringPinSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static FiringPinSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern). Use the static {@link #getInstance()} method to obtain a reference to the subsystem. For
     * example, instead of doing this:
     * <pre>
     *     FiringPinSubsystem subsystem = new FiringPinSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     FiringPinSubsystem subsystem = FiringPinSubsystem.getInstance();
     * </pre>
     */
    private FiringPinSubsystem()
    {
        super(RobotMap2013.Solenoids.FIRING_PIN_FORWARD_ADDR, RobotMap2013.Solenoids.FIRING_PIN_RETRACT_ADDR);
    }


    public boolean isEnabled()
    {
        return isEnabled;
    }


    public void fire()
    {
        engage();
    }


    public void postFireReset()
    {
        disengage();
    }
}
