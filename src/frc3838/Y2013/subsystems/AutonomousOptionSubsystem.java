package frc3838.Y2013.subsystems;


import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.components.TriSwitch;
import frc3838.Y2013.components.TriSwitchPosition;
import frc3838.Y2013.utils.LOG;



public class AutonomousOptionSubsystem extends Subsystem
{

    public static boolean isEnabled = true;

    private static final AutonomousOptionSubsystem singleton = new AutonomousOptionSubsystem();

    private TriSwitch triSwitch;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new AutonomousOptionSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     AutonomousOptionSubsystem subsystem = new AutonomousOptionSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     AutonomousOptionSubsystem subsystem = AutonomousOptionSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static AutonomousOptionSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     AutonomousOptionSubsystem subsystem = new AutonomousOptionSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     AutonomousOptionSubsystem subsystem = AutonomousOptionSubsystem.getInstance();
     * </pre>
     */
    private AutonomousOptionSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in AutonomousOptionSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled)
        {
            try
            {
                LOG.debug("Initializing AutonomousOptionSubsystem");
                triSwitch = new TriSwitch(RobotMap2013.DigitalIO.AUTONOMOUS_MODE_TRI_SWITCH_1_CHANNEL, RobotMap2013.DigitalIO.AUTONOMOUS_MODE_TRI_SWITCH_2_CHANNEL);
                RobotMap2013.autonomousOption = triSwitch.getState();
                LOG.info("Autonomous Switch state: " + triSwitch.getState());
                LOG.debug("AutonomousOptionSubsystem initialization completed successfully");

            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in AutonomousOptionSubsystem.init()", e);
            }
        }
        else
        {
            LOG.info("AutonomousOptionSubsystem is disabled and will not be initialized");
        }
    }


    public TriSwitchPosition getAutonomousOption()
    {
        return triSwitch.getState();
    }


    public String getOptionAsDetailedString()
    {
        return triSwitch.getStateDetails();
    }


    public void initDefaultCommand()
    {
        // TODO: Set the default command for the subsystem here.
        //setDefaultCommand(new MyCommand());
    }
}
