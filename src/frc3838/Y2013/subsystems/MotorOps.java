package frc3838.Y2013.subsystems;


import edu.wpi.first.wpilibj.SpeedController;
import frc3838.Y2013.utils.math.Math2;



/** @noinspection UnusedDeclaration */
class MotorOps
{
    // Motor settings
    //    setRaw() ranges from 0 to 255. 128 to 255 is forward speed, and 127 to 0 is reversed
    //    set() ranges from -1 to 1 and is provided for victor.

    private static final double STOP = 0;

    private boolean inverse;
    private int inverseFactor;

    private double minNonStopSpeed = 0.01;

    /** The speed <b>without</b> alteration for forward or reversed. */
    private double speed;

    private SpeedController controller;


    MotorOps(SpeedController speedController, boolean inverse)
    {
        this.controller = speedController;
        this.inverse = inverse;
        inverseFactor = inverse ? -1 : 1;
        speedController.set(STOP);
    }


    MotorOps(SpeedController speedController, boolean inverse, double minNonStopSpeed)
    {
        this.controller = speedController;
        this.inverse = inverse;
        inverseFactor = inverse ? -1 : 1;
        this.minNonStopSpeed = minNonStopSpeed;
        speedController.set(STOP);
    }


    /** @param speed the speed (between -1 and 1) such that a positive speed is forward, a negative speed is reverse, and zero is stopped. */
    public String setSpeed(double speed)
    {
        this.speed = speed;
        return setSpeedToCurrentSetting();
    }


    public void setMinNonStopSpeed(double minNonStopSpeed)
    {
        this.minNonStopSpeed = minNonStopSpeed;
    }


    public String setSpeedToCurrentSetting()
    {
        if (Math.abs(speed) < minNonStopSpeed)
        {
            speed = 0;
        }
        controller.set(speed * inverseFactor);
        return getSpeedPercentage();
    }


    public String stop()
    {
        //We do not set the speed field to zero as we want to retain the last speed used for a restart.
        controller.set(STOP);
        return getSpeedPercentage();
    }


    public void setForward10Percent() { setSpeed(0.1);}


    public void setForward25Percent() { setSpeed(0.25); }


    public void setForward50Percent() { setSpeed(0.5); }


    public void setForward75Percent() { setSpeed(0.75); }


    public void setForwardFull() { setSpeed(1.0); }


    public void setReverse10Percent() { setSpeed(-0.1); }


    public void setReverse25Percent() { setSpeed(-0.25); }


    public void setReverse50Percent() { setSpeed(-0.5); }


    public void setReverse75Percent() { setSpeed(-0.75); }


    public void setReverseFull() { setSpeed(-1.0); }


    public String increaseForwardSpeedOnePercent() { return increaseForwardSpeed(.01); }


    public String increaseForwardSpeedThreePercent() { return increaseForwardSpeed(.03); }


    public String increaseForwardSpeedFivePercent() { return increaseForwardSpeed(.05); }


    public String increaseForwardSpeedTenPercent() { return increaseForwardSpeed(.1); }


    public String decreaseForwardSpeedOnePercent() { return decreaseForwardSpeed(.01); }


    public String decreaseForwardSpeedThreePercent() { return decreaseForwardSpeed(.03); }


    public String decreaseForwardSpeedFivePercent() { return decreaseForwardSpeed(.05); }


    public String decreaseForwardSpeedTenPercent() { return decreaseForwardSpeed(.1); }


    /**
     * Increases the forward speed by the specified amount, but does nto allow the motor to go into reverse. For example, if the current speed is 5% (i.e. 0.05) forward and a value
     * of change of 8% (i.e. 0.08) is passed in, the motor speed will be set to 0, not 3% reversed.
     *
     * @param delta the amount to decrease as a value between 0 and 1.
     *
     * @return
     */
    public String decreaseForwardSpeed(double delta)
    { return modifySpeed(-Math.abs(delta)); }


    public String increaseForwardSpeed(double delta) { return modifySpeed(Math.abs(delta));}


//    public String increaseSpeedTenPercent() {return modifyForwardSpeed(); }


    public boolean isRunning()
    {
        return controller.get() != 0;
    }


    private String modifySpeed(double delta)
    {
        double realDelta = delta * inverseFactor;
        double current = controller.get();
        double newSpeed = current + realDelta;

        speed = constrainValue(newSpeed);
        return setSpeedToCurrentSetting();
    }


    /**
     * Constrains the the speed to between -1 and 1. Note that this method does <b>not</b> set the speed field or modify the motor's speed. It is simply a math helper method.
     *
     * @param speed the speed to constrain
     *
     * @return the constrained speed
     */
    public double constrainValue(double speed)
    {
        if (speed > 1)
        {
            return 1;
        }
        else if (speed < -1)
        {
            return -1;
        }
        else
        {
            return speed;
        }
    }


    public String getSpeedPercentage()
    {
        if (controller.get() == 0)
        {
            return "STOPPED";
        }
        return isForward() ? "F:" + Math2.toPercentage(controller.get()) : "R:" + Math2.toPercentage(controller.get());
    }


    public boolean isForward()
    {

        return inverse ? controller.get() < 0 : controller.get() > 0;
    }
}
