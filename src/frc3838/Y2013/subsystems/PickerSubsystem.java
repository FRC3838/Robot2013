package frc3838.Y2013.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Victor;
import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.components.SolenoidSubsystem;
import frc3838.Y2013.utils.LOG;



public class PickerSubsystem extends SolenoidSubsystem
{
    public static boolean isEnabled = true;
    private static final PickerSubsystem singleton = new PickerSubsystem();
    public static boolean isInPulseMode = false;


    private DigitalInput activateSwitch;
    private MotorOps pickerMotorOps;

    //0 to 1
    private double speed = 1;

    private double slowEjectSpeed = 0.8;
    private double fastEjectSpeed = 1;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such as <tt>new PickerSubsystem()</tt> in order to use ensure only a single
     * instance of a Subsystem is created. This is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     PickerSubsystem subsystem = new PickerSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     PickerSubsystem subsystem = PickerSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static PickerSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern). Use the static {@link #getInstance()} method to obtain a reference to the subsystem. For
     * example, instead of doing this:
     * <pre>
     *     PickerSubsystem subsystem = new PickerSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     PickerSubsystem subsystem = PickerSubsystem.getInstance();
     * </pre>
     */
    private PickerSubsystem()
    {
        super(RobotMap2013.Solenoids.PICKER_DOWN_ADDR, RobotMap2013.Solenoids.PICKER_UP_ADDR);
        additionalInit();
    }


    private void additionalInit()
    {
        try
        {
            if (isEnabled)
            {
                pickerMotorOps = new MotorOps(new Victor(RobotMap2013.PWMChannels.PICKER_MOTOR), true);
                activateSwitch = new DigitalInput(RobotMap2013.DigitalIO.PICKER_LIMIT_SWITCH);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in PickerSubsystem.additionalInit()", e);
        }
    }


    public DigitalInput getActivateSwitch()
    {
        return activateSwitch;
    }


    public void startMotor()
    {
        isInPulseMode = false;
        pickerMotorOps.setSpeed(speed);
    }


    public void stopMotor()
    {
        isInPulseMode = false;
        pickerMotorOps.stop();
    }


    public void pauseMotor()
    {
        pickerMotorOps.stop();
    }


    public boolean isEnabled()
    {
        return isEnabled;
    }


    public void lowerPicker()
    {
        engage();
    }


    public void slowEject()
    {
        isInPulseMode = true;
        pickerMotorOps.setSpeed(slowEjectSpeed);
    }


    public boolean isPulsing()
    {
        return isInPulseMode;
    }


    public void raisePicker()
    {
        disengage();
    }
}
