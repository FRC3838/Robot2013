/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/
package frc3838.Y2013;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import frc3838.Y2013.commands.CommandBase;
import frc3838.Y2013.commands.autonomous.AutonomousCenterCommandGroup;
import frc3838.Y2013.commands.autonomous.AutonomousLeftCommandGroup;
import frc3838.Y2013.commands.autonomous.AutonomousRightCommandGroup;
import frc3838.Y2013.components.TriSwitchPosition;
import frc3838.Y2013.subsystems.CompressorSubsystem;
import frc3838.Y2013.utils.LOG;



/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to each mode, as described in the IterativeRobot documentation. If you change the
 * name of this class or the package after creating this project, you must also update the manifest file in the resource directory.
 *
 * @noinspection RefusedBequest
 */
public class TheRobot2013 extends IterativeRobot
{

    Command activeAutonomousCommand;


    /** This function is run when the robot is first started up and should be used for any initialization code. */
    public void robotInit()
    {
        try
        {
            LOG.debug("entering robotInit method");
            // Note: the original template has the call to CommandBase.init() coming after creating the
            //        autonomousCommand and any teleop commands. But there is the possibility that a command
            //        requires a subsystem. In that case, the subsystem would be null. So it would seem that
            //        the call to init() must come first. It shouldn't break anything coming first.


            // Initialize all subsystems
            CommandBase.init();

            // instantiate the command used for the autonomous period

            //MAY BE NULL!!!!
            TriSwitchPosition autonomousOption = RobotMap2013.autonomousOption;

            LOG.info("Switch position read as: " + autonomousOption);
            if (TriSwitchPosition.FlippedRight.equals(autonomousOption))
            {
                LOG.info("Running autonomous option 'Right'");
                activeAutonomousCommand = new AutonomousRightCommandGroup("AutonomousRightCommandGroup");
            }
            else if (TriSwitchPosition.CenterPosition.equals(autonomousOption))
            {
                LOG.info("Running autonomous option 'Center'");
                activeAutonomousCommand = new AutonomousCenterCommandGroup("AutonomousCenterCommandGroup");
            }
            else
            {
                LOG.info("Running autonomous option 'Left'");
                activeAutonomousCommand = new AutonomousLeftCommandGroup("AutonomousLeftCommandGroup");
            }
            // instantiate commands used for the teleop period

        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TheRobot2013.robotInit", e);
        }


        LOG.debug("exit robotInit method");
    }


    public void autonomousInit()
    {
        try
        {
            RobotMap2013.inAutonomousMode = true;
            // schedule the autonomous command (example)

            try
            {
                CompressorSubsystem.getInstance().stopCompressor();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in TheRobot2013.autonomousInit()", e);
            }

            activeAutonomousCommand.start();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TheRobot2013.autonomousInit()", e);
        }
        finally
        {
            RobotMap2013.inAutonomousMode = false;
        }
    }


    /** This function is called periodically during autonomous */
    public void autonomousPeriodic()
    {
        try
        {
            Scheduler.getInstance().run();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TheRobot2013.autonomousPeriodic()", e);
        }
    }


    public void teleopInit()
    {
        // This makes sure that the autonomous stops running when
        // teleop starts running. If you want the autonomous to
        // continue until interrupted by another command, remove
        // this line or comment it out.
        try
        {
            CompressorSubsystem.getInstance().startCompressorAndAutoRun();

            activeAutonomousCommand.cancel();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when canceling the activeAutonomousCommand", e);
        }
    }


    /** This function is called periodically during operator control */
    public void teleopPeriodic()
    {
        try
        {
            Scheduler.getInstance().run();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TheRobot2013.teleopPeriodic()", e);
        }
    }

}
