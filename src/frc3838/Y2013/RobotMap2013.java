package frc3838.Y2013;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc3838.Y2013.components.TriSwitchPosition;
import frc3838.Y2013.utils.LogLevel;



/**
 * The Robot2013Map is a mapping from the ports sensors and actuators are wired into to a variable name. This provides flexibility changing wiring, makes checking the wiring easier
 * and significantly reduces the number of magic numbers floating around.
 *
 * @noinspection UtilityClassWithoutPrivateConstructor
 */
public class RobotMap2013
{


    private static final int unassigned = -99;

    public static boolean inAutonomousMode = false;

    public static TriSwitchPosition autonomousOption;


    //private constructor to prevent instantiation as all access should be to static members
    private RobotMap2013()
    { }


    public static final LogLevel LOG_LEVEL = LogLevel.INFO;
    public static final boolean IN_DEBUG_MODE = false;


    public static final class AnalogIO
    {

    }

    public static final class Constants
    {

    }

    /** @noinspection UnusedDeclaration */
    public static final class DigitalIO
    {
        public static final int COMPRESSOR_PRESSURE_SWITCH_CHANNEL = 1;
        public static final int AUTONOMOUS_MODE_TRI_SWITCH_1_CHANNEL = 2;
        public static final int AUTONOMOUS_MODE_TRI_SWITCH_2_CHANNEL = 3;
        public static final int PICKER_LIMIT_SWITCH = 4;

        //left claw limit switch - indicates claw is in an up position and  left motor should not run in an up direction
        public static final int LEFT_CLAW_UP_POSITION_SWITCH_CHANNEL = 5;

        //right claw limit switch indicates claw is un up position and right motor should not run in an up direction
        public static final int RIGHT_CLAW_UP_POSITION_SWITCH_CHANNEL = 7;
    }

    public static final class I2CBus
    {

    }

    public static final class PWMChannels
    {
//        public static final int LEFT_REAR_DRIVE_MOTOR = unassigned;
//        public static final int LEFT_FRONT_DRIVE_MOTOR = unassigned;
//        public static final int RIGHT_FRONT_DRIVE_MOTOR = unassigned;
//        public static final int RIGHT_REAR_DRIVE_MOTOR = unassigned;

        public static final int LEFT_DRIVE_MOTOR = 1;
        public static final int RIGHT_DRIVE_MOTOR = 2;

        public static final int UPPER_SHOOTER_MOTOR = 3;
        public static final int LOWER_SHOOTER_MOTOR = 4;

        public static final int CLIMBER_ARMS_MOTOR = 5;
        public static final int PICKER_MOTOR = 6;
    }

    public static final class Relays
    {
        public static final int COMPRESSOR_RELAY_CHANNEL = 1;
    }

    public static final class Solenoids
    {
        public static final int GEAR_BOX_CLIMB_ENGAGE_ADDR = 1;
        public static final int GEAR_BOX_CLIMB_DISENGAGE_ADDR = 2;

        public static final int FIRING_PIN_RETRACT_ADDR = 3;
        public static final int FIRING_PIN_FORWARD_ADDR = 4;

        public static final int PICKER_UP_ADDR = 5;
        public static final int PICKER_DOWN_ADDR = 6;

        public static final int SHOOTER_ANGLE_ENGAGE_ADDR = 7;
        public static final int SHOOTER_ANGLE_DISENGAGE_ADDR = 8;
    }

    public static final class Joysticks
    {
        public static final int DRIVER_JOYSTICK_PORT = 1;
        public static final int OPS_LEFT_JOYSTICK_PORT = 2;
        public static final int OPS_RIGHT_JOYSTICK_PORT = 3;


        public static final Joystick OPS_LEFT_JOYSTICK = new Joystick(OPS_LEFT_JOYSTICK_PORT);
        public static final Joystick OPS_RIGHT_JOYSTICK = new Joystick(OPS_RIGHT_JOYSTICK_PORT);
        public static final Joystick DRIVER_JOYSTICK = new Joystick(DRIVER_JOYSTICK_PORT);
    }


    public static final class Buttons
    {
        private static final Joystick opsLeftJoystick = Joysticks.OPS_LEFT_JOYSTICK;
        private static final Joystick opsRightJoystick = Joysticks.OPS_RIGHT_JOYSTICK;
        private static final Joystick driverJoystick = Joysticks.DRIVER_JOYSTICK;


        //         JOYSTICK BUTTON LAYOUT
        // **************************************
        // *              (Trigger)             *
        // *          .................         *
        // *          :               :         *
        // * +---+    :     +---+     :   +---+ *
        // * | 6 |    . +-+ | 3 | +-+ :   + 11| *
        // * +---+    : |4| +---+ |5| :   +---+ *
        // *          : +-+ +---+ +-+ :         *
        // * +---+    :     | 2 |     :   +---+ *
        // * | 7 |    :     +---+     :   | 10| *
        // * +---+    :...............:   +---+ *
        // * Left          Stick          Right *
        // * Group      +---+  +---+      Group *
        // *            | 8 |  | 9 |            *
        // *            +---+  +---+            *
        // *            Bottom Group            *
        // **************************************

        // ------------------------------------------------------------------------------------------------

        /* ====  OPS LEFT JOYSTICK ===  */

        // (See diagram below)
        // Joystick buttons:
        //  1 Trigger
        public static final JoystickButton SHOOT = new JoystickButton(opsLeftJoystick, 1);
        //  2 Bottom Thumb
        public static final JoystickButton CHANGE_TO_LOW_SHOT = new JoystickButton(opsLeftJoystick, 2);
        //  3 Top Thumb
        public static final JoystickButton CHANGE_TO_HIGH_SHOT = new JoystickButton(opsLeftJoystick, 3);
        //  4 Left Thumb
        public static final JoystickButton START_SHOOTER_MOTORS = new JoystickButton(opsLeftJoystick, 4);
        //  5 Right Thumb
        public static final JoystickButton STOP_SHOOTER_MOTORS = new JoystickButton(opsLeftJoystick, 5);
        //  6 Left Group Front
        public static final JoystickButton RAISE_PICKER = new JoystickButton(opsLeftJoystick, 6);
        //  7 Left Group Back
        public static final JoystickButton LOWER_PICKER = new JoystickButton(opsLeftJoystick, 7);
        //  8 Bottom Group Left
        public static final JoystickButton PULSE_HOLD_EJECT = new JoystickButton(opsLeftJoystick, 8);
        //  9 Bottom Group Right
        public static final JoystickButton SLOW_PICKER_EJECT = new JoystickButton(opsLeftJoystick, 9);
        // 10 Right Group Back
        public static final JoystickButton START_PICKER_MOTOR = new JoystickButton(opsLeftJoystick, 10);
        // 11 Right Group Front
        public static final JoystickButton STOP_PICKER_MOTOR = new JoystickButton(opsLeftJoystick, 11);

        // ------------------------------------------------------------------------------------------------

         /* ====  OPS LEFT JOYSTICK ===  */

        // (See diagram below)
        // Joystick buttons:
        //  1 Trigger
        public static final JoystickButton SHOOT2 = new JoystickButton(opsRightJoystick, 1);
        //  2 Bottom Thumb
        public static final JoystickButton SHOOTER_SPEED_DOWN_TEN = new JoystickButton(opsRightJoystick, 2);
        //  3 Top Thumb
        public static final JoystickButton SHOOTER_SPEED_UP_TEN = new JoystickButton(opsRightJoystick, 3);
        //  4 Left Thumb
        public static final JoystickButton SHOOTER_SPEED_DOWN_ONE = new JoystickButton(opsRightJoystick, 4);
        //  5 Right Thumb
        public static final JoystickButton SHOOTER_SPEED_UP_ONE = new JoystickButton(opsRightJoystick, 5);
        //  6 Left Group Front
        public static final JoystickButton CLIMBER_ARM_DOWN = new JoystickButton(opsRightJoystick, 6);
        //  7 Left Group Back
        public static final JoystickButton CLIMBER_ARM_UP = new JoystickButton(opsRightJoystick, 7);
        //  8 Bottom Group Left
        //  9 Bottom Group Right
        // 10 Right Group Back
        // 11 Right Group Front


        // ------------------------------------------------------------------------------------------------

         /* ====  DRIVER  JOYSTICK ===  */

        // (See diagram below)
        // Joystick buttons:
        //  1 Trigger
        public static final JoystickButton SENSITIVITY_TOGGLE = new JoystickButton(driverJoystick, 1);
        //  2 Bottom Thumb
        public static final JoystickButton DISENGAGE_CLIMB_MODE = new JoystickButton(driverJoystick, 2);
        //  3 Top Thumb
        public static final JoystickButton ENGAGE_CLIMB_MODE = new JoystickButton(driverJoystick, 3);
        //  4 Left Thumb
        //  5 Right Thumb
        //  6 Left Group Front
        //  7 Left Group Back
        //  8 Bottom Group Left
        //  9 Bottom Group Right
        // 10 Right Group Back
        // 11 Right Group Front
        public static final JoystickButton TOGGLE_STICK_MODE = new JoystickButton(driverJoystick, 11);


        // ------------------------------------------------------------------------------------------------


        //         JOYSTICK BUTTON LAYOUT
        // **************************************
        // *              (Trigger)             *
        // *          .................         *
        // *          :               :         *
        // * +---+    :     +---+     :   +---+ *
        // * | 6 |    . +-+ | 3 | +-+ :   + 11| *
        // * +---+    : |4| +---+ |5| :   +---+ *
        // *          : +-+ +---+ +-+ :         *
        // * +---+    :     | 2 |     :   +---+ *
        // * | 7 |    :     +---+     :   | 10| *
        // * +---+    :...............:   +---+ *
        // * Left          Stick          Right *
        // * Group      +---+  +---+      Group *
        // *            | 8 |  | 9 |            *
        // *            +---+  +---+            *
        // *            Bottom Group            *
        // **************************************

    }

}
